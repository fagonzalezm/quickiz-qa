package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import grupo.uno.quickiz.controllers.QuestionsController;
import grupo.uno.quickiz.models.Question;
import grupo.uno.quickiz.repositories.QuestionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value=QuestionsController.class, secure=false)
@MockBean()
public class QuestionRestControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@Mock
	private QuestionRepository questionRepository;
	
	@Test
	public void createQuestionTest() throws Exception {
		Question mockQuestion = new Question();
		mockQuestion.setId(1);
		mockQuestion.setTitle("¿Qué imprime el siguiente código por pantalla?");
		
		//COnvertir el objeto a Json
		String inputInJson = this.mapToJson(mockQuestion);
		
		//Url del metodo create
		String URI = "/questions";
		
		//Cuando se cree cualquier tipo de Pool retorne "mockPool".
		Mockito.when(questionRepository.save(Mockito.any(Question.class))).thenReturn(mockQuestion);
		
		//Constructor del objeto JSON.
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(inputInJson)
				.contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		String outputInJson = response.getContentAsString();
		
		assertThat(outputInJson).isEqualTo(inputInJson);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		
	}
	
	/**
	 * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
	 */
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
	
	
	

}
