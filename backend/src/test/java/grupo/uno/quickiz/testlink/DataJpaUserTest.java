package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import grupo.uno.quickiz.models.User;
import grupo.uno.quickiz.repositories.UserRepository;

@RunWith(SpringRunner.class)
@DataJpaTest 
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DataJpaUserTest {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void userSaveTest() {
		User user = getUser("Prueba1" );
		User userToDB = em.persist(user);
		em.flush();
		
		User userFromDB = userRepository.findById(userToDB.getId()).orElse(null);
		
		assertThat(userFromDB.getName()).isEqualTo(user.getName());
	}
	
	@Test
	public void geUsertByIdTest() {
		User user = getUser("Prueba2" );
		
		//Guardar user en la base de datos.
		User userToDB = em.persist(user);
		em.flush();
		
		//Obtenerlo de la base de datos.
		User userFromDB = userRepository.findById(userToDB.getId()).orElse(null);
		
		assertThat(userToDB).isEqualTo(userFromDB);
	}
	
	@Test
	public void getAllUserTest() {
		User user = getUser("Prueba3" );
		User user2 = getUser("Prueba4" );
		User user3 = getUser("Prueba5" );
		
		//Guardar user, user2 y user3 en la base de datos.
		em.persist(user);
		em.persist(user2);
		em.persist(user3);
		
		
		//Obtener todos los user desde la base de datos.
		List<User> allUserFromDB = userRepository.findAll();
		List<User> userList = new ArrayList<>();
		
		for(User useri : allUserFromDB) {
			userList.add(useri);
		}
		
		assertThat(userList.size()).isEqualTo(allUserFromDB.size());
	}
	
	@Test
	public void deleteUserByIdTest() {
		User user = getUser("Prueba6" );
		User user2 = getUser("Prueba7" );
		
		//Guardar user y user2 en la base de datos.
		User userToDD = em.persist(user);
		em.persist(user2);
		em.flush();
		
		//Obtener todos los user desde la base de datos.
		List<User> allUserFromDB = userRepository.findAll();
		
		//ELiminar userToDb.
		em.remove(userToDD);
		
		//Obtener todos los user desde la base de datos tras eliminar userToDB.
		List<User> allUserFromDB2 = userRepository.findAll();
		
		List<User> userList = new ArrayList<>();
		
		for(User useri : allUserFromDB2) {
			userList.add(useri);
		}
		
		assertThat(userList.size()).isEqualTo((allUserFromDB.size()-1));
	}
	
	@Test
	public void updateUserTest() {
		User user = getUser("Prueba8" );
		
		//Guardar user  en la base de datos.
		em.persist(user);
		em.flush();
		
		//Obtener user desde la base de datos.
		User userFromDB = userRepository.findById(user.getId()).orElse(null);
		
		//Actualizar name de userFromDb.
		userFromDB.setName("Prueba9");
		em.persist(userFromDB);
		
		
		assertThat(userFromDB.getName()).isEqualTo("Prueba9");
	}
	
	public User getUser(String name) {
		User user = new User();
		user.setName(name);
		return user;
		
	}


}
