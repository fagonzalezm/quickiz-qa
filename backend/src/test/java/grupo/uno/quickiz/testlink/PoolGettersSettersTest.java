package grupo.uno.quickiz.testlink;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import grupo.uno.quickiz.models.Pool;
import grupo.uno.quickiz.models.Question;
import grupo.uno.quickiz.models.QuestionStructure;

@RunWith(SpringRunner.class)
public class PoolGettersSettersTest {
	
	@Test
	public void getSetNameTest() {
		Pool pool = new Pool();
        pool.setName("Enteros, reales y operaciones aritméticas.");
        assertTrue(pool.getName() == "Enteros, reales y operaciones aritméticas.");
	}
	
	@Test
	public void getSetIdTest() {
		Pool pool = new Pool();
		pool.setId(1);
        pool.setName("Enteros, reales y operaciones aritméticas.");
        assertTrue(pool.getId() == 1);
	}
	
	@Test
	public void getSetQuestionTest() {
		List<Question> questionList = new ArrayList<>();
		
		Question question = new Question();
		question.setId(1);
        question.setTitle("¿Qué muestra el siguiente código por pantalla.");
        
        Pool pool = new Pool();
		pool.setId(1);
        pool.setName("Enteros, reales y operaciones aritméticas.");
        
        questionList.add(question);
        pool.setQuestions(questionList);
        
        assertTrue(pool.getQuestions() == questionList);
	}
	
	@Test
	public void getSetQuestionStructureTest() {
		List<QuestionStructure> questionStructureList = new ArrayList<>();
		
		Pool pool = new Pool();
		pool.setId(1);
        pool.setName("Enteros, reales y operaciones aritméticas.");
        
        QuestionStructure questionStructure = new QuestionStructure();
        questionStructure.setId(1);
        questionStructure.setPool(pool);
        questionStructure.setScore(3);
        
        questionStructureList.add(questionStructure);
        pool.setQuestionStructures(questionStructureList);
        
        assertTrue(pool.getQuestionStructures() == questionStructureList);
	}
	
	

}
