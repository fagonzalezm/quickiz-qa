package grupo.uno.quickiz.models;

import java.util.*;
import lombok.AccessLevel;
import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Question;
import grupo.uno.quickiz.models.QuestionStructure;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "pools")
@SuppressWarnings("serial")
public class Pool implements Serializable {

    @Id
    @GeneratedValue(
    strategy = GenerationType.AUTO
    )
    @Column(
    name = "id",
    unique = true,
    nullable = false
    )
    private Integer id;

    @Column(name = "name")
    private String name;

    @JsonBackReference
    @OneToMany
    private List<Question> questions = new ArrayList<>();

    @JsonBackReference
    @OneToMany
    private List<QuestionStructure> questionStructures = new ArrayList<>();

    public List<Question> getQuestions() {
        return this.questions;
    }

    public List<QuestionStructure> getQuestionStructures() {
        return this.questionStructures;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public void setQuestionStructures(List<QuestionStructure> questionStructures) {
        this.questionStructures = questionStructures;
    }

}

