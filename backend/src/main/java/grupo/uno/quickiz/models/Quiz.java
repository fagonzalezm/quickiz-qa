package grupo.uno.quickiz.models;

import java.util.*;
import javax.persistence.*;
import java.io.Serializable;
import org.joda.time.DateTime;
import grupo.uno.quickiz.models.QuizStructure;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "quizzes")
@SuppressWarnings("serial")
public class Quiz implements Serializable {

    @Id
    @GeneratedValue(
    strategy = GenerationType.AUTO
    )
    @Column(
    name = "id",
    unique = true,
    nullable = false
    )
    private Integer id;

    @Column(name = "start_at")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime startAt;

    @Column(name = "finish_at")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime finishAt;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="quiz_structure_id")
    private QuizStructure quizStructure;

    // @JsonBackReference
    // @OneToMany
    // private List<QuestionStructure> questionStructures = new ArrayList<>();

    // public List<QuestionStructure> getQuestionStructures() {
        // return this.questionStructures;
    // }

    // public void setQuestionStructures(List<QuestionStructure> questionStructures) {
        // this.questionStructures = questionStructures;
    // }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public DateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(DateTime startAt) {
        this.startAt = startAt;
    }

    public DateTime getFinishAt() {
        return finishAt;
    }

    public void setFinishAt(DateTime finishAt) {
        this.finishAt = finishAt;
    }

    public QuizStructure getQuizStructure() {
        return quizStructure;
    }

    public void setQuizStructure(QuizStructure quizStructure) {
        this.quizStructure = quizStructure;
    }

}

