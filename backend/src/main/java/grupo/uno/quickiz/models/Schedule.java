package grupo.uno.quickiz.models;

import javax.persistence.*;
import org.joda.time.DateTime;

@Entity
@Table(name = "schedules")
public class Schedule {

    @Id
    @GeneratedValue(
    strategy = GenerationType.AUTO
    )
    @Column(
    name = "id",
    unique = true,
    nullable = false
    )
    private Integer id;

    @Column(name = "code")
    private String code;

    @Column(name = "start_at")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime startAt;

    @Column(name = "finish_at")
    // @Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    private DateTime finishAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DateTime getStartAt() {
        return startAt;
    }

    public void setStartAt(DateTime startAt) {
        this.startAt = startAt;
    }

    public DateTime getFinishAt() {
        return finishAt;
    }

    public void setFinishAt(DateTime finishAt) {
        this.finishAt = finishAt;
    }

}
