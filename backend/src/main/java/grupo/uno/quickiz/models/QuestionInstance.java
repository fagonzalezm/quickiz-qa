package grupo.uno.quickiz.models;

import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Quiz;
import grupo.uno.quickiz.models.Question;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "question_instance")
@SuppressWarnings("serial")
public class QuestionInstance implements Serializable {

    @Id
    @GeneratedValue(
    strategy = GenerationType.AUTO
    )
    @Column(
    name = "id",
    unique = true,
    nullable = false
    )
    private Integer id;

    @Column(name = "answer")
    private String answer;

    @Column(name = "variable_values")
    private String variableValues; // Json

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="question_id")
    private Question question;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getVariableValues() {
        return variableValues;
    }

    public void setVariableValues(String variableValues) {
        this.variableValues = variableValues;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

}

